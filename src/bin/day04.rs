extern crate chrono;
extern crate regex;

use chrono::{NaiveDateTime, Timelike};
use docopt::Docopt;
use regex::Regex;
use std::collections::HashMap;
use std::fs;
use std::io::{BufRead, BufReader, Write};

const USAGE: &'static str = "
Advent of Code 2018 - Day 4

Usage:
    day04 <input> [-v | --verbose] [-d | --debug]
    day04 (-h | --help)

Options:
    -h, --help      Show this screen
    -v, --verbose   Verbose output
    -d, --debug     Debug output to debug.txt
";

#[derive(Debug)]
struct Log {
    date: NaiveDateTime,
    msg: String,
}

struct Shift {
    /// hour shift representation in minutes, where 0=awake, 1=asleep
    time_asleep: [u8; 60],
    /// Minutes asleep during this shift
    min_asleep: u16,
}

struct GuardSleep {
    id: u16,
    min_asleep: u16,
}

fn parse_lines(lines: Vec<String>, verbose: bool) -> Result<Vec<Log>, &'static str> {
    let mut log: Vec<Log> = Vec::new();
    let re = Regex::new(r"^\[(.*)\] (.*)").unwrap();
    for l in lines {
        let caps = re.captures(&l).unwrap();
        let date =
            NaiveDateTime::parse_from_str(caps.get(1).unwrap().as_str(), "%Y-%m-%d %H:%M").unwrap();
        let logentry = caps.get(2).unwrap().as_str();
        let entry = Log {
            date: date,
            msg: logentry.to_string(),
        };
        if verbose {
            println!("{:?}", entry);
        }
        log.push(entry);
    }
    log.sort_unstable_by_key(|k| k.date.clone());
    Ok(log)
}

fn get_guard_id(re: &Regex, msg: &String) -> Option<u32> {
    re.captures(msg)
        .and_then(|c| c.get(1))
        .and_then(|c| c.as_str().parse().ok())
}

fn process_log(log: Vec<Log>) -> Result<HashMap<u32, Vec<Shift>>, String> {
    let mut guards: HashMap<u32, Vec<Shift>> = HashMap::new();
    let re = Regex::new(r"^Guard #(\d+)").unwrap();

    let mut it = log.iter();
    let mut i: u32 = 0;
    let mut first = true;
    let mut id = 0u32;
    loop {
        // get a line from the log
        let mut l = match it.next() {
            Some(line) => line,
            None => break,
        };

        id = match get_guard_id(&re, &l.msg) {
            // guard id found, means new shift started
            Some(id) => {
                // increment index
                i += 1;
                // and get the next line
                l = match it.next() {
                    Some(line) => line,
                    None => break,
                };
                // return id
                id
            }
            None => {
                if first {
                    panic!("Expected guard ID. Not found");
                } else {
                    // return previous id
                    id
                }
            }
        };

        if first {
            first = false;
        }

        // get shifts of current guard id
        let shifts = guards.entry(id).or_insert(Vec::new());

        // guard fell asleep
        let sleep = if l.msg.trim() == "falls asleep" {
            l.date
        } else {
            continue;
        };

        // get next line
        i += 1;
        l = match it.next() {
            Some(line) => line,
            None => panic!(format!(
                "{}: Expected time guard woke up. Found end of list",
                i
            )),
        };

        // guard woke up
        let wake = if l.msg.trim() == "wakes up" {
            l.date
        } else {
            return Err(format!("{}: Expected wake up, found: {}", i, l.msg));
        };

        // how long was guard asleep
        let min_asleep = match (wake - sleep).to_std().map(|d| d.as_secs()) {
            Ok(d) => d,
            Err(_) => return Err(format!("{}: Unable to calc duration", i)),
        };

        let mut shift = Shift {
            time_asleep: [0; 60],
            min_asleep: min_asleep as u16 / 60,
        };
        for m in sleep.minute()..wake.minute() {
            shift.time_asleep[m as usize] = 1;
        }

        shifts.push(shift);

        i += 1;
    }
    Ok(guards)
}

fn main() {
    let args = Docopt::new(USAGE)
        .and_then(|d| d.parse())
        .unwrap_or_else(|e| e.exit());

    let input = args.get_str("<input>");
    let verbose = args.get_bool("--verbose");
    let debug = args.get_bool("--debug");

    println!("Day 4");
    println!("  Input: {}", input);
    let file = BufReader::new(fs::File::open(input).unwrap());
    let lines: Vec<_> = file.lines().map(|x| x.unwrap()).collect();
    let log = parse_lines(lines, verbose).unwrap();

    if debug {
        let mut f = fs::File::create("sorted.txt").unwrap();
        for (i, l) in log.iter().enumerate() {
            f.write(format!("{}: {} {}\n", i, l.date, l.msg).as_bytes())
                .unwrap();
        }
    }

    let guards = match process_log(log) {
        Ok(guards) => guards,
        _ => panic!(),
    };

    // find guard that slept the most
    let mut guard_slept: Vec<GuardSleep> = Vec::new();
    for (id, shifts) in guards.iter() {
        let mut sum = 0;
        for s in shifts {
            sum += s.min_asleep;
        }
        guard_slept.push(GuardSleep {
            id: *id as u16,
            min_asleep: sum,
        });
    }
    guard_slept.sort_unstable_by_key(|g| g.min_asleep);
    for g in guard_slept.iter() {
        if verbose {
            println!("Guard {} slept a total of {} minutes", g.id, g.min_asleep);
        };
    }

    let sleepy = match guard_slept.last() {
        Some(sleepy) => sleepy,
        None => panic!("Error getting last element from list"),
    };

    println!(
        "  Guard {} slept the most: {} minutes",
        sleepy.id, sleepy.min_asleep
    );

    // find the minute he slept the most
    let sleepy_shifts = &guards[&(sleepy.id as u32)];

    let mut minutes = [0u8; 60];
    for shift in sleepy_shifts {
        for (i, m) in shift.time_asleep.iter().enumerate() {
            minutes[i] += m;
        }
    }

    // find minute that was most spent asleep
    let mut max_min = 0u8;
    let mut max_min_asleep = 0u8;
    for (i, m) in minutes.iter().enumerate() {
        if *m > max_min_asleep {
            max_min_asleep = *m;
            max_min = i as u8;
        }
    }
    println!(
        "  Minute {} was spent asleep {} minutes",
        max_min, max_min_asleep
    );
    println!(
        "  Answer (guard x minute): {} x {} = {}",
        sleepy.id, max_min, (sleepy.id as u32)*(max_min as u32));


    println!("");
    println!("  Part 2");
    let mut gid = 0u32;
    let mut minute = 0u8;
    let mut min_asleep = 0u8;
    for (id, shifts) in guards {
        let mut minutes = [0u8; 60];
        for shift in shifts {
            for (i, m) in shift.time_asleep.iter().enumerate() {
                minutes[i] += m;
            }
        }

        let mut max_min_asleep = 0u8;
        let mut max_min = 0u8;
        for (i, m) in minutes.iter().enumerate() {
            if *m > max_min_asleep {
                max_min_asleep = *m;
                max_min = i as u8;
            }
        }

        if max_min_asleep > min_asleep {
            min_asleep = max_min_asleep;
            minute = max_min;
            gid = id;
        }
    }

    println!(
        "  Guard {} spent minute {} asleep {} minutes",
        gid, minute, min_asleep
    );

    println!(
        "  Answer (guard_id x minute): {} x {} = {}",
        gid, minute, (gid as u32)*(minute as u32));
}
